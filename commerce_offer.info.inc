<?php

/**
 * @file
 * Provides metadata for the commerce_store and commerce_offer entity types.
 */

/**
 * Implements hook_entity_property_info().
 */
function commerce_offer_entity_property_info() {
  $info = array();

  $store_properties = &$info['commerce_store']['properties'];

  $store_properties['store_id'] = array(
    'label' => t('Store ID'),
    'description' => t('The internal numeric ID of the store.'),
    'type' => 'integer',
  );
  $store_properties['name'] = array(
    'label' => t('Name'),
    'description' => t('The name of the store.'),
    'type' => 'text',
    'setter callback' => 'entity_metadata_verbatim_set',
    'schema field' => 'name',
    'required' => TRUE,
  );
  $store_properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the store was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer products',
    'schema field' => 'created',
  );

  $offer_properties = &$info['commerce_offer']['properties'];

  $offer_properties['offer_id'] = array(
    'label' => t('Offer ID'),
    'description' => t('The internal numeric ID of the offer.'),
    'type' => 'integer',
    'schema field' => 'offer_id',
  );
  $offer_properties['product_id'] = array(
    'label' => t('Product ID'),
    'type' => 'integer',
    'description' => t("The ID of the referenced product."),
    'setter callback' => 'entity_metadata_verbatim_set',
    'setter permission' => 'administer offers',
    'clear' => array('product'),
    'schema field' => 'product_id',
  );
  $offer_properties['product'] = array(
    'label' => t('Product'),
    'type' => 'commerce_product',
    'description' => t("The referenced product."),
    'getter callback' => 'commerce_offer_get_properties',
    'setter callback' => 'commerce_offer_set_properties',
    'setter permission' => 'administer offers',
    'clear' => array('product_id'),
  );
  $offer_properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the offer was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer offers',
    'schema field' => 'created',
  );
  $offer_properties['changed'] = array(
    'label' => t('Date changed'),
    'description' => t('The date the offer was most recently updated.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer offers',
    'schema field' => 'changed',
  );

  return $info;
}
