<?php

/**
 * @file
 * Rules integration for offers.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_offer_rules_action_info() {
  $actions = array();

  $actions['commerce_offer_unit_price_set'] = array(
    'label' => t("Set the unit price to lowest offered in the current store"),
    'parameter' => array(
      'line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line item'),
      ),
    ),
    'group' => t('Commerce Line Item'),
  );

  return $actions;
}

/**
 * Rules action: return the proper unit price amount.
 */
function commerce_offer_unit_price_set($line_item, $amount) {
  $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $product_id = $wrapper->commerce_product->product_id->value();
  $product_type = $wrapper->commerce_product->type->value();
  // This product type doesn't have offers enabled.
  if (!commerce_offer_product_type_enabled($product_type)) {
    return;
  }

  $offer = commerce_offer_get_lowest_offer($product_id);
  if (isset($offer)) {
    // Substitute the amount everywhere for the one from the offer.
    $offer_wrapper = entity_metadata_wrapper('commerce_offer', $offer);
    $new_amount = $offer_wrapper->commerce_offer_price->amount->value();

    $wrapper->commerce_unit_price->amount = $new_amount;
    $base_price = $wrapper->commerce_unit_price->data->value();
    $base_price['components'][0]['price']['amount'] = $new_amount;
    $wrapper->commerce_unit_price->data = $base_price;
  }
}
