<?php

/**
 * @file
 * Provides forms for creating, updating, and deleting offers
 */

/**
 * Form callback wrapper: create an offer.
 *
 * @param $product The parent product.
 *
 * @see commerce_offer_form()
 */
function commerce_offer_add_offer_page($product) {
  drupal_set_title(t('Create an offer'));
  
  // Add the breadcrumb for the form's location.
  commerce_offer_ui_offer_set_breadcrumb($product);

  // Initialize an offer.
  $offer = entity_create('commerce_offer', array());

  return drupal_get_form('commerce_offer_form', $offer, $product);
}

/**
 * Form callback wrapper: edit an offer.
 *
 * @param $offer The offer entity.
 *
 * @see commerce_offer_form()
 */
function commerce_offer_edit_offer_page($offer) {
  // Load the full variation.
  $product = commerce_product_load($offer->product_id);

  // Add the breadcrumb for the form's location.
  commerce_offer_ui_offer_set_breadcrumb($product);

  return drupal_get_form('commerce_offer_form', $offer, $product);
}

/**
 * Form callback: create or edit an offer.
 *
 * @param $form
 * @param $form_state
 * @param $offer The offer entity, if an edit action is happening.
 * @param $product The product entity referenced in the offer.
 */
function commerce_offer_form($form, &$form_state, $offer, $product) {
  $form_state['offer'] = $offer;
  $form_state['product'] = $product;

  // Give other modules a chance to add some fields to the form.
  field_attach_form('commerce_offer', $offer, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#weight' => 1000,
    '#attributes' => array('class' => array('form-actions')),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save offer'),
    '#suffix' => l('Cancel', 'admin/commerce/products/' . $product->product_id . '/offers/'),
  );

  return $form;
}

/**
 * Form submit handler: save the offer.
 */
function commerce_offer_form_submit($form, &$form_state) {
  $offer = $form_state['offer'];
  $product = $form_state['product'];

  $offer->product_id = $product->product_id;
  // Notify field widgets.
  field_attach_submit('commerce_offer', $offer, $form, $form_state);

  $offer->save();
  drupal_set_message(t('Your offer has been saved.'));
  $form_state['redirect'] = 'admin/commerce/products/' . $product->product_id . '/offers/';
}

/**
 * Form callback wrapper: delete an offer.
 *
 * @param $offer The offer entity.
 *
 * @see commerce_offer_delete_form()
 */
function commerce_offer_delete_offer_page($offer) {
  $product = commerce_product_load($offer->product_id);

  // Add the breadcrumb for the form's location.
  commerce_offer_ui_offer_set_breadcrumb($product);

  return drupal_get_form('commerce_offer_delete_form', $offer, $product);
}

/**
 * Confirmation form for deleting an offer.
 */
function commerce_offer_delete_form($form, &$form_state, $offer, $product) {
  $form_state['offer'] = $offer;
  $form_state['product'] = $product;

  $form = confirm_form($form,
    t('Are you sure you want to delete this offer?'),
    'admin/commerce/products/' . $product->product_id . '/offers/',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Form submit handler: delete the offer.
 */
function commerce_offer_delete_form_submit($form, &$form_state) {
  $offer = $form_state['offer'];
  $product = $form_state['product'];

  $offer->delete();

  drupal_set_message(t('The offer has been deleted.'));
  $form_state['redirect'] = 'admin/commerce/products/' . $product->product_id . '/offers/';
}

/**
 * Sets the breadcrumb for offer forms.
 */
function commerce_offer_ui_offer_set_breadcrumb($product) {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Store'), 'admin/commerce'),
    l(t('Products'), 'admin/commerce/products'),
    l(t('Product: @sku', array('@sku' => $product->sku)), 'admin/commerce/products/' . $product->product_id),
    l(t('Offers'), 'admin/commerce/products/' . $product->product_id . '/offers'),
  );

  drupal_set_breadcrumb($breadcrumb);
}
