<?php

/**
 * Field handler to present operations from contextual links.
 */
class commerce_offer_handler_field_operations extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);

    $links = menu_contextual_links('commerce-offer', $this->definition['router item'], array($value));

    if (!empty($links)) {
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
