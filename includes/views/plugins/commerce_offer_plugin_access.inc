<?php

/**
 * Access plugin used for the Offers view.
 *
 * Assumes that the view is located at:
 * admin/commerce/products/%commerce_product/offers
 */
class commerce_offer_plugin_access extends views_plugin_access {
  function access($account) {
    return commerce_offer_access('view', arg(3));
  }

  function get_access_callback() {
    return array('commerce_offer_access', array('view', 3));
  }

  function summary_title() {
    return t('Offers');
  }
}
