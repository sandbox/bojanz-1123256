<?php

/**
 * @file
 * Provides the store & offer listing views.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_offer_ui_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'stores';
  $view->description = 'Displays a list of all defined stores.';
  $view->tag = 'default';
  $view->base_table = 'commerce_store';
  $view->human_name = 'Stores';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Stores';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer stores';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'operations' => 'operations',
  );
  $handler->display->display_options['style_options']['default'] = 'name';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'operations' => array(
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 1;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No stores have been created yet.';
  /* Field: Commerce store: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'commerce_store';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  /* Field: Commerce store: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'commerce_store';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['label'] = 'Operations';
  $handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['operations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['operations']['empty_zero'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/commerce/config/stores/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Stores';
  $handler->display->display_options['tab_options']['description'] = 'Manage stores providing offers for products.';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';
  $translatables['stores'] = array(
    t('Master'),
    t('Stores'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('No stores have been created yet.'),
    t('Name'),
    t('Operations'),
    t('Page'),
  );
  $views[$view->name] = $view;

  $view = new view;
  $view->name = 'offers';
  $view->description = 'Displays a list of all defined offers.';
  $view->tag = 'default';
  $view->base_table = 'commerce_offer';
  $view->human_name = 'Offers';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Offers';
  $handler->display->display_options['access']['type'] = 'commerce_offer_access';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'commerce_store' => 'commerce_store',
    'commerce_offer_price' => 'commerce_offer_price',
    'operations' => 'operations',
  );
  $handler->display->display_options['style_options']['default'] = 'commerce_store';
  $handler->display->display_options['style_options']['info'] = array(
    'commerce_store' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'commerce_offer_price' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'operations' => array(
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 1;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No offers have been created yet.';
  /* Field: Commerce offer: Store */
  $handler->display->display_options['fields']['commerce_store']['id'] = 'commerce_store';
  $handler->display->display_options['fields']['commerce_store']['table'] = 'field_data_commerce_store';
  $handler->display->display_options['fields']['commerce_store']['field'] = 'commerce_store';
  $handler->display->display_options['fields']['commerce_store']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_store']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_store']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_store']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_store']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_store']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_store']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_store']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_store']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_store']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_store']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_store']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_store']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_store']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_store']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_store']['field_api_classes'] = 0;
  /* Field: Commerce offer: Price */
  $handler->display->display_options['fields']['commerce_offer_price']['id'] = 'commerce_offer_price';
  $handler->display->display_options['fields']['commerce_offer_price']['table'] = 'field_data_commerce_offer_price';
  $handler->display->display_options['fields']['commerce_offer_price']['field'] = 'commerce_offer_price';
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_offer_price']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_offer_price']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_offer_price']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_offer_price']['type'] = 'commerce_price_formatted_amount';
  $handler->display->display_options['fields']['commerce_offer_price']['settings'] = array(
    'calculation' => FALSE,
  );
  $handler->display->display_options['fields']['commerce_offer_price']['field_api_classes'] = 0;
  /* Field: Commerce offer: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'commerce_offer';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['label'] = 'Operations';
  $handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['operations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['operations']['empty_zero'] = 0;
  /* Contextual filter: Commerce offer: Product ID */
  $handler->display->display_options['arguments']['product_id']['id'] = 'product_id';
  $handler->display->display_options['arguments']['product_id']['table'] = 'commerce_offer';
  $handler->display->display_options['arguments']['product_id']['field'] = 'product_id';
  $handler->display->display_options['arguments']['product_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['product_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['product_id']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['product_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['product_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['product_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['product_id']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['product_id']['not'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/commerce/products/%/offers';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Offers';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'devel';
  $translatables['offers'] = array(
    t('Master'),
    t('Offers'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('No offers have been created yet.'),
    t('Store'),
    t('Price'),
    t('Operations'),
    t('All'),
    t('Page'),
  );
  $views[$view->name] = $view;

  return $views;
}
