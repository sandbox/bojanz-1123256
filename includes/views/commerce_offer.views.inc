<?php

/**
 * @file
 * Provides additional Views integration (the "operations" fields).
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_offer_views_data_alter(&$data) {
  $data['commerce_store']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'real field' => 'store_id',
      'router item' => 'admin/commerce/config/stores',
      'help' => t('Display all the available operations links for the store.'),
      'handler' => 'commerce_offer_handler_field_operations',
    ),
  );
  $data['commerce_offer']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'real field' => 'offer_id',
      'router item' => 'admin/commerce/offers',
      'help' => t('Display all the available operations links for the offer.'),
      'handler' => 'commerce_offer_handler_field_operations',
    ),
  );
}

/**
 * Implements hook_views_plugins().
 */
function commerce_offer_views_plugins() {
  return array(
    'access' => array(
      'commerce_offer_access' => array(
        'title' => t('Offers'),
        'handler' => 'commerce_offer_plugin_access',
        'uses options' => FALSE,
      ),
    ),
  );
}
