<?php

/**
 * @file
 * Default rule configurations for Offers.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_offer_default_rules_configuration() {
  $rules = array();
  // Add a reaction rule to update an order to the default status of the pending
  // order status upon checkout completion.
  $rule = rules_reaction_rule();
  $rule->label = t('Use the lowest offer price instead of the product price for the line item');
  $rule->active = TRUE;

  $rule
    ->event('commerce_product_calculate_sell_price')
    ->action('commerce_offer_unit_price_set', array(
      'line_item:select' => 'line_item',
    ));

  $rules['commerce_offer_lowest_price'] = $rule;

  return $rules;
}
